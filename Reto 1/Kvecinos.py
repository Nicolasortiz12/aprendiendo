import pandas as pd
import numpy as np
from math import ceil

from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import classification_report, confusion_matrix

from tkinter import *
from tkinter import messagebox
from tkinter import ttk
from tkinter import filedialog


df=pd.read_csv(r"C:\\Users\\Anderson\\Desktop\\SPE\\2021\\Hackathon\\202104\\BASE\\base_train_data.csv")
#atributos
col=['OILGRAV','SOLGOR','Visco','Bo','FLUIDTYPE']
#Dataset
df = df[col]
#Limpieza del dataset
df = df.dropna(subset=['OILGRAV','Visco','SOLGOR','Bo','FLUIDTYPE'], how='any')

Tipos=["Full_Saturated", "Highly_Unsaturated", "Near_Saturation", "Undersaturated", "Very_Highly_Undersaturated"]

#Seleccion de variables
X= df.iloc[:,:-1].values
Y=df.iloc[:,4].values

#Division de datos en entrenamiento y testeo
x_E, x_T, y_E, y_T = train_test_split(X, Y, test_size=0.1, random_state = 4)

#Normalizacion
scaler = StandardScaler()
scaler.fit(x_E)

x_E = scaler.transform(x_E)
x_T = scaler.transform(x_T)

#Modelo
classifier = KNeighborsClassifier(n_neighbors=5)
classifier.fit(x_E, y_E)
#Predicciones
y_pred = classifier.predict(x_T)
#Evaluacion
print(confusion_matrix(y_T, y_pred))
print(classification_report(y_T, y_pred))


#Aplicacion sobre los datos de Prueba
df_T=pd.read_csv(r"C:\\Users\\Anderson\\Desktop\\SPE\\2021\\Hackathon\\202104\\BASE\\base_test_data.csv")
df_T = df_T[['OILGRAV','SOLGOR','Visco','Bo','FLUIDTYPE']]
#df_T.replace('NaN', np.nan, inplace=True)
df_T.round(2)
mean=df_T[['OILGRAV','SOLGOR','Visco','Bo']].mean()
mean.round(2)
df_T = df_T.fillna(mean)
#df_T[['OILGRAV','SOLGOR','Visco','Bo']].replace(np.nan, mean)


#df_T = df_T.dropna(subset=['OILGRAV','Visco','SOLGOR','Bo'], how='any')

df_T = df_T[['OILGRAV','SOLGOR','Visco','Bo']]
#Seleccion de variables de los datos de Prueba
X= df_T.iloc[:,:].values
X = scaler.transform(X)
Y_Prueba = classifier.predict(X)

X=pd.DataFrame(X)
Y_Prueba=pd.DataFrame(Y_Prueba)
DF_P = pd.concat([X, Y_Prueba],axis=1)
DF_P.columns=['OILGRAV','Visco','SOLGOR','Bo','Tipo de Fluido (Modelo)']
print(DF_P)


Ventana=Tk()
Ventana.title("Datos de Prueba")
frame=Frame(Ventana).pack()
class Tree_View:
    def _init_(self, ventana,Datos):
        self.TV=ttk.Treeview(ventana)
        self.TV.place(relheight=1, relwidth=1)
        barra_y = Scrollbar(ventana, orient="vertical", command=self.TV.yview)
        barra_x = Scrollbar(ventana, orient="horizontal", command=self.TV.xview)
        self.TV.configure(xscrollcommand=barra_x.set, yscrollcommand=barra_y.set)
        barra_y.pack(side="right", fill="y")
        barra_x.pack(side="bottom", fill="x")
        self.TV["columns"] = list(Datos.columns)
        self.TV["show"] = "headings"
        for column in self.TV["columns"]:
            self.TV.heading(column, text=column)
        for row in Datos.to_numpy().tolist():
            self.TV.insert("", "end", values=row)
Tree_View(Ventana, DF_P)

export_file_path = filedialog.asksaveasfilename(defaultextension='.csv')
DF_P.to_csv (export_file_path, index = False, header=True)

Ventana.mainloop()