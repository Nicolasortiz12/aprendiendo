import pandas as pd
import numpy as np
import pylab as pl
import scipy.optimize as opt
import sklearn
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from sklearn import svm
from sklearn.metrics import f1_score
from sklearn.metrics import jaccard_score
from tkinter import *
from tkinter import messagebox
from tkinter import ttk
from tkinter import filedialog

df=pd.read_csv(r"C:\\Users\\Anderson\\Desktop\\SPE\\2021\\Hackathon\\202104\\BASE\\base_train_data.csv")
col=['CASENAME','OILGRAV','SOLGOR','Visco','Bo','FLUIDTYPE']
df = df[col]
df = df.dropna(subset=['OILGRAV','Visco','SOLGOR','Bo','FLUIDTYPE'], how='any')

Tipos=["Full_Saturated", "Highly_Unsaturated", "Near_Saturation", "Undersaturated", "Very_Highly_Undersaturated"]
def Clasificador (df, Tipo):
    #1 va a ser la salida si FLUIDTYPE es igual a Tipo
    df['Tipo'] = np.where(df['FLUIDTYPE']==Tipo, 1, 2)
    return df
"""
for i in range(0, len(Tipos))
    df=Clasificador(df,_Tipos[i])
"""
df=Clasificador(df,Tipos[3])
#Creacion de variables independientes
caracteristicas=df[['OILGRAV','SOLGOR','Visco','Bo']]
X = np.asarray(caracteristicas)
X[0:5]
#Creacion de variables dependientes
df['Tipo']=df['Tipo'].astype('int')
Y = np.asarray(df['Tipo'])
Y[0:5]

#Division de datos en entrenamiento y testeo
x_E, x_T, y_E, y_T = train_test_split(X, Y, test_size=0.2, random_state = 4 )
#Creacion del modelo y su entrenamiento

modelo=svm.SVC(kernel='rbf')
modelo.fit(x_E, y_E)

#Prediccion
yf = modelo.predict(x_T)

print("F1: ", str(f1_score(y_T,yf,average="weighted")))
print("Jaccard: ", str(jaccard_score(y_T,yf,pos_label=2)))



#Aplicarlo sobre los datos de prueba
df_T=pd.read_csv(r"C:\\Users\\Anderson\\Desktop\\SPE\\2021\\Hackathon\\202104\\BASE\\base_test_data.csv")
df_T = df_T[col]
df_T = df_T.dropna(subset=['OILGRAV','Visco','SOLGOR','Bo'], how='any')
df_T=Clasificador(df_T,Tipos[3])
#sacamos a X de los datos de prueba
caracteristicas=df_T[['OILGRAV','SOLGOR','Visco','Bo']]
P = np.asarray(caracteristicas)
P[0:5]

#Sacamos a Y de los datos de prueba
df_T['Tipo']=df_T['Tipo'].astype('int')
Y_P = np.asarray(df_T['Tipo'])
Y_P[0:5]

#Generamos la prediccion sobre los datos de prueba
y_P=modelo.predict(P)

# Hacemos un dataframe con los datos obtenidos para montarlo en el Treeview

datos_x_T = pd.DataFrame(P)
datos_yf = pd.DataFrame(y_P)
DF_P = pd.concat([datos_x_T, datos_yf],axis=1)

columnas_T=['OILGRAV','SOLGOR','Visco','Bo', 'Tipo de fluido (Modelo)']
DF_P.columns=columnas_T

DF_P['Tipo de fluido (Modelo)'] = np.where(DF_P['Tipo de fluido (Modelo)']==1, "Undersaturated", "Otros")


Ventana=Tk()
Ventana.title("Datos de Prueba")
frame=Frame(Ventana).pack()
class Tree_View:
    def _init_(self, ventana,Datos):
        self.TV=ttk.Treeview(ventana)
        self.TV.place(relheight=1, relwidth=1)
        barra_y = Scrollbar(ventana, orient="vertical", command=self.TV.yview)
        barra_x = Scrollbar(ventana, orient="horizontal", command=self.TV.xview)
        self.TV.configure(xscrollcommand=barra_x.set, yscrollcommand=barra_y.set)
        barra_y.pack(side="right", fill="y")
        barra_x.pack(side="bottom", fill="x")
        self.TV["columns"] = list(Datos.columns)
        self.TV["show"] = "headings"
        for column in self.TV["columns"]:
            self.TV.heading(column, text=column)
        for row in Datos.to_numpy().tolist():
            self.TV.insert("", "end", values=row)
Tree_View(Ventana, DF_P)
Ventana.mainloop()